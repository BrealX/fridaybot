# -*- coding: utf-8 -*-

from typing import Any, Dict, List, Optional

import apiai
import datetime
import json
import os
import random
import requests

from bottle import Bottle, response, request as bottle_request
from decouple import config


class BotHandlerMixin:
    BOT_URL: Optional[str] = None
    WEATHER_API_URL: Optional[str] = None
    PRIVATBANK_API_URL: Optional[str] = None
    BOT_NAME: Optional[str] = None

    @staticmethod
    def get_data() -> Dict[str, Any]:
        """
        Method to extract all data from telegram request.
        """

        return bottle_request.json

    @staticmethod
    def get_chat_id(data: Dict[str, Any]) -> str:
        """
        Method to extract chat id from telegram request.
        """

        try:
            chat_id = data['message']['chat']['id']
        except KeyError:
            chat_id = 'empty'
        return chat_id

    @staticmethod
    def get_message(data: Dict[str, Any]) -> str:
        """
        Method to extract message id from telegram request.
        """

        try:
            message_text = data['message'].get('text')
        except KeyError:
            message_text = ''
        return message_text

    @staticmethod
    def get_username(data: Dict[str, Any]) -> str:
        """
        Method to extract user posted name from telegram request.
        """

        try:
            username = data['message']['from']['first_name']
        except KeyError:
            username = ''
        return username

    @staticmethod
    def get_username_id(data: Dict[str, Any]) -> str:
        """
        Method to extract user id from telegram request.
        """

        try:
            user_id = data['message']['from']['id']
        except KeyError:
            user_id = ''
        return user_id

    def send_message(self, prepared_data: Dict[str, Any]) -> None:
        """
        Prepared data should be json which includes at least `chat_id` and `text`
        """

        message_url = self.BOT_URL + 'sendMessage'
        requests.post(message_url, json=prepared_data)

    @staticmethod
    def clean_message_from_punctuation(message: str) -> List[str]:
        result = []
        message_words = message.split()

        for word in message_words:
            clean_word = ''
            for char in word:
                if char in '!,.?":;0123456789':
                    char = ''
                clean_word += char
            result.append(clean_word)
        return result


class FridayBot(BotHandlerMixin, Bottle):
    BOT_URL: str = f'https://api.telegram.org/bot{config("TELEGRAM_API_BOT_TOKEN")}/'
    WEATHER_API_URL: str = f'http://api.openweathermap.org/data/2.5/weather?'
    PRIVATBANK_API_URL: str = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5'
    BOT_NAME: str = 'фрай'

    greetings = ('здравствуй', 'привет', 'ку', 'добрый день', 'доброе утро',
                 'добрый вечер', 'здарова', 'ку-ку', 'приветули', 'доброй ночи', 'хай', 'hello')
    commands = ('пятница', 'перевыбор', 'частичка', 'команды', 'курс', 'погода')
    employees = ('Аня', 'Сергей', 'Катя', 'Витя', 'Олег', 'Лёша', 'Никита', 'Артём', 'Андрей', 'Даша')
    admin_employees = ('Аня', 'Сергей', '300404532', )

    def __init__(self, *args, **kwargs):
        super(FridayBot, self).__init__()
        self.route('/', callback=self.post_handler, method="POST")
        self.today = datetime.datetime.now().day
        self.city_name = 'Kyiv'
        self.helloed_users = set()
        self.next_organizator = str()
        self.next_part = str()
        self.next_friday = datetime.date.today() - datetime.timedelta(7)

    def greet(self, message: str = '', user: str = '') -> str:
        """
        Method to greet users. Greeting is generated once per each day per user.
        To another greeting message from user bot responds with custom text but not greeting again.
        :param message: incoming message from telegram request
        :param user: user that sent incoming message from telegram request
        :return: greeting message
        """

        now = datetime.datetime.now()
        day = now.day
        hour = now.hour
        answer = str()

        if day != self.today:
            self.helloed_users = set()
            self.today = day

        message_words = message.split()
        message_in_greetings = False
        message_is_sent_today = day == self.today
        morning_message = 4 <= hour < 11
        day_message = 11 <= hour < 17
        evening_message = 17 <= hour < 23
        night_message = 0 <= hour < 4
        user_was_already_greeted = user in self.helloed_users

        for word in message_words:
            if word in self.greetings:
                message_in_greetings = True

        if message_words and user:
            if message_in_greetings and message_is_sent_today and morning_message and not user_was_already_greeted:
                answer = f'Доброе утро, {user}!'
            elif message_in_greetings and message_is_sent_today and day_message and not user_was_already_greeted:
                answer = f'Добрый день, {user}!'
            elif message_in_greetings and message_is_sent_today and evening_message and not user_was_already_greeted:
                answer = f'Добрый вечер, {user}'
            elif message_in_greetings and message_is_sent_today and night_message and not user_was_already_greeted:
                answer = f'Доброй ночи, {user}. Не спится?'
            self.helloed_users.add(user)
        return answer

    def is_admin(self, user_id: str) -> bool:
        return str(user_id) in self.admin_employees

    def introduce(self) -> str:
        return (
            f'Меня зовут {self.BOT_NAME.title()}. Если ты хочешь со мной поговорить, '
            f'то знай: я отвечу, только если обратиться ко мне по имени.\n\n '
            f'Сейчас я понимаю несколько команд, а именно:\n '
            f'"пятница" - я помогу тебе определить человека, который будет отвечать за '
            f'организацию игр в следующую пятницу\n '
            f'"частичка" - я назначу человека, который должен будет принести частичку себя в офис\n '
            f'"перевыбор" - если по какой-либо причине выбранный человек не сможет организовать игру в '
            f'пятницу, я помогу переизбрать нового счастливчика\n '
            f'"курс" - подскажу тебе актуальные наличные курсы валют на сегодня в Приватбанке\n'
            f'"погода" - покажу погоду на текущий момент в Киеве\n\n '
            f'А еще со мной можно просто поболтать, '
            f'правда я еще только учусь понимать людей и могу нести иногда чушь...\n '
            f'В общем, пиши "Фрай, ..." и мы поговорим.'
        )

    def check_commands(self, message: str = '', user_id: str = '') -> str:
        """
        Method checks incoming message for commands presence and calls appropriate function with it.
        :param message: incoming message from telegram request
        :param user_id: user who sent a message from telegram request
        :return: depending on command returns generated answer
        """

        answer = ''
        message_words = self.clean_message_from_punctuation(message)
        commands = [word for word in message_words if word in self.commands]

        if len(commands) > 1:
            answer = 'Ты меня запутать хочешь? Или хочешь много знать? Выбери только одну команду'
        elif len(commands) == 1:
            if commands[0] == 'пятница':
                answer = self.randomize(friday=True)
            elif commands[0] == 'перевыбор':
                answer = self.randomize_again(user_id, friday=True)
            elif commands[0] == 'частичка':
                answer = self.randomize(part=True)
            elif commands[0] == 'команды':
                answer = self.introduce()
            elif commands[0] == 'курс':
                answer = self.get_currency()
            elif commands[0] == 'погода':
                answer = self.get_weather()
        return answer

    def randomize(self, friday: bool = False, part: bool = False) -> str:
        today = datetime.date.today()

        if today <= self.next_friday and not part:
            answer = self.random_employee(once_more=True, friday=friday, part=part)
        else:
            answer = self.random_employee(friday=friday, part=part)
            while today.weekday() != 4:
                today += datetime.timedelta(1)
            self.next_friday = today

        return answer

    def randomize_again(self, user_id: str, friday: bool = False, part: bool = False) -> str:
        answer = str()
        user_is_admin = self.is_admin(user_id)
        if user_is_admin:
            answer = self.random_employee(friday=friday, part=part)
        return answer

    def random_employee(self, once_more: bool = False, friday: bool = False, part: bool = False) -> str:
        pre = ''

        if friday:
            pre = (
                'Барабанная дробь... Победитель - ',
                'На этот раз везунчик - ',
                'В пятницу организатор игр ',
                'В номинации "Организатор недели" побеждает ',
                'Здесь все: Никита, Стас, Гена, Турбо и Дюша Метёлкин... А в пятницу организатор - ',
                'Ladiiiiies and gentlemeeeeeen! The winner is - ',
                'В пятницу придумывает игру и вкусняшки '
            )
        elif part:
            pre = ('Частичку себя в офис приносит ', )

        if pre:
            salt = random.choice(pre)
            employee = random.choice(self.employees)
            while employee == self.next_organizator:
                employee = random.choice(self.employees)
            if once_more:
                return f'Организатор уже выбран - {self.next_organizator}'

            self.next_organizator = employee
            return salt + employee
        else:
            return ''

    def get_currency(self) -> str:
        resp = requests.get(self.PRIVATBANK_API_URL).json()
        now = datetime.datetime.now()
        answer = f'Наличные курсы валют в Приватбанке на {now.strftime("%d-%m-%Y %H:%M:%S")}:\n\n'

        for currency in resp:
            answer += f'{currency.get("ccy")}: покупка {currency.get("buy")}, продажа {currency.get("sale")}\n'
        return answer

    def get_weather(self) -> str:
        weather_token = config('OPENWEATHER_API_TOKEN')
        params = {'q': self.city_name, 'type': 'like', 'units': 'metric', 'appid': weather_token, 'lang': 'ru'}
        resp = requests.get(self.WEATHER_API_URL, params=params).json()
        return (
            f'На {datetime.datetime.fromtimestamp(resp.get("dt"))} в Киеве '
            f'{[d.get("description") for d in resp.get("weather")][0]}. '
            f'Температура воздуха {int(float(resp.get("main").get("temp")))} C°, '
            f'ветер {resp.get("wind").get("speed")} м/с'
        )

    def prepare_data_for_answer(self, data: Dict[str, Any]) -> Dict[str, Any]:
        """
        Method for generating answers for users.
        Minimum required fields for response are: 'chat_id' and 'text'
        :param data: telegram request data (self.get_data())
        :return: response to telegram
        """

        last_message = self.get_message(data).lower()
        last_username = self.get_username(data)
        last_username_id = self.get_username_id(data)

        answer = ''
        if self.BOT_NAME in last_message:
            greetings = self.greet(last_message, last_username)
            commands = self.check_commands(last_message, last_username_id)
            if greetings:
                answer = greetings
            elif commands:
                answer = commands
            else:
                request = apiai.ApiAI(config('APIAI_TOKEN')).text_request()
                request.lang = 'ru'
                request.session_id = 'BatlabAIBot'
                request.query = last_message
                response_json = json.loads(request.getresponse().read().decode('utf-8'))
                r = response_json['result']['fulfillment']['speech']
                answer = r or 'Я тебя не совсем понял!'
        return {
            'chat_id': self.get_chat_id(data),
            'text': answer,
        }

    def post_handler(self) -> response:
        data = self.get_data()
        answer_data = self.prepare_data_for_answer(data)
        self.send_message(answer_data)

        return response


if __name__ == '__main__':
    app = FridayBot()
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 8000)))
